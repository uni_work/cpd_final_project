package com.claudiutoader.project.ui.controller;

import com.claudiutoader.project.configuration.RabbitMQConfigurationC2;
import com.claudiutoader.project.model.Message;
import com.claudiutoader.project.pub_sub.PublisherC2;
import com.claudiutoader.project.ui.view.ClientViewC2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class ControllerC2 implements Observer{
    private ClientViewC2 clientViewC2;
    private PublisherC2 publisherC2;

    @Autowired
    public ControllerC2(ClientViewC2 clientViewC2, PublisherC2 publisherC2) {
        this.clientViewC2 = clientViewC2;
        this.publisherC2 = publisherC2;

        clientViewC2.sportButtonListener(new SportButtonListener());
        clientViewC2.cookingButtonListener(new CookingButtonListener());
        clientViewC2.harryPotterButtonListener(new HarryPotterListener());
    }

    public void addTextToTextArea(String text) {
        clientViewC2.addTextToTextArea(text);
        clientViewC2.repaint();
    }

    @Override
    public void update(boolean status) {
        clientViewC2.setSportButtonStatus(status);
//        clientViewC2.setCookingButtonStatus(status);
        clientViewC2.setHarryPotterButtonStatus(status);
        clientViewC2.repaint();
    }

    class SportButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientViewC2.getPubText(), "", RabbitMQConfigurationC2.ROUTING_KEY_SPORT);
            publisherC2.sendMessage(message, RabbitMQConfigurationC2.ROUTING_KEY_SPORT);
        }
    }

    class CookingButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientViewC2.getPubText(), "", RabbitMQConfigurationC2.ROUTING_KEY_COOKING);
            publisherC2.sendMessage(message, RabbitMQConfigurationC2.ROUTING_KEY_COOKING);
        }
    }

    class HarryPotterListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientViewC2.getPubText(), "", RabbitMQConfigurationC2.ROUTING_KEY_HARRY_POTTER);
            publisherC2.sendMessage(message, RabbitMQConfigurationC2.ROUTING_KEY_HARRY_POTTER);
        }
    }


}
