package com.claudiutoader.project.server;


import com.claudiutoader.project.configuration.ServerSocketConfigurationC2;
import com.claudiutoader.project.ui.controller.ControllerC2;
import com.claudiutoader.project.ui.controller.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


@Service
public class TokenRingServerC2 implements Subject, CommandLineRunner {
//    private final RabbitTemplate rabbitTemplate;
    private BufferedReader input;
    private PrintWriter output;
    private ServerSocket serverIn;
    private Socket serverOut;
    private ControllerC2 controllerC2;
    private boolean status;


    @Autowired
    public TokenRingServerC2(ControllerC2 controllerC2) throws IOException {
        this.controllerC2 = controllerC2;
        serverIn = new ServerSocket(ServerSocketConfigurationC2.PORT_6060);
    }


    public void start() throws IOException, InterruptedException {
        serverOut = null;
        while (true) {
            try {
                serverOut = new Socket(ServerSocketConfigurationC2.IP, ServerSocketConfigurationC2.PORT_7070);
                if (serverOut != null) { break; }
            }
            catch (IOException e) { Thread.sleep(1000); }
        }

        Socket receiver = serverIn.accept();
        input = new BufferedReader(new InputStreamReader(receiver.getInputStream()));
        output = new PrintWriter(serverOut.getOutputStream(), true);

        String token = "";
        notifySubscribers();
        while(true){

            if(token.equalsIgnoreCase("token")){
                status = true;
                notifySubscribers();

                long startTime = System.currentTimeMillis();

                System.out.println("Token received! Server running: " + serverIn.getLocalPort());
                while(System.currentTimeMillis() - startTime < 10000){
                    // change status for 10 seconds
                }

                status = false;
                notifySubscribers();
                output.println("token");

            }
            System.out.println("Waiting for token...");
            token = input.readLine();

        }
    }

    public void stop() throws IOException {
        serverIn.close();
        serverOut.close();
    }

    @Override
    public void notifySubscribers() {
        controllerC2.update(status);
    }

    @Override
    public void run(String... args) throws Exception {
        this.start();
    }
}
