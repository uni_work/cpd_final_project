package com.claudiutoader.project.pub_sub;

import com.claudiutoader.project.configuration.RabbitMQConfigurationC2;
import com.claudiutoader.project.model.Message;
import com.claudiutoader.project.ui.controller.ControllerC2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ReceiverC2 {

    private final ControllerC2 controllerC2;

    @Autowired
    public ReceiverC2(ControllerC2 controllerC2) {
        this.controllerC2 = controllerC2;
    }

    @RabbitListener(queues = {RabbitMQConfigurationC2.QUEUE_C2},
            containerFactory = "listenerContainerFactory")
    public void receiveMessage(final Message message)  {
        controllerC2.addTextToTextArea(message.toString());
    }

    /*
    public void receiveMessage(final Message message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        controller.addTextToTextArea(message.toString());

        try {
            channel.basicAck(tag, false);
        } catch (Exception e) {
            channel.basicNack(tag, false, true);
        }
    }
    */

}
