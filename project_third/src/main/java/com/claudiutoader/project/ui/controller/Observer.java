package com.claudiutoader.project.ui.controller;

public interface Observer {
    void update(boolean status);
}
