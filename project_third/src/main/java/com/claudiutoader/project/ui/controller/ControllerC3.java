package com.claudiutoader.project.ui.controller;

import com.claudiutoader.project.configuration.RabbitMQConfigurationC3;
import com.claudiutoader.project.model.Message;
import com.claudiutoader.project.pub_sub.PublisherC3;
import com.claudiutoader.project.ui.view.ClientViewC3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class ControllerC3 implements Observer{
    private ClientViewC3 clientViewC3;
    private PublisherC3 publisherC3;

    @Autowired
    public ControllerC3(ClientViewC3 clientViewC3, PublisherC3 publisherC3) {
        this.clientViewC3 = clientViewC3;
        this.publisherC3 = publisherC3;

        clientViewC3.sportButtonListener(new SportButtonListener());
        clientViewC3.cookingButtonListener(new CookingButtonListener());
        clientViewC3.harryPotterButtonListener(new HarryPotterListener());
    }

    public void addTextToTextArea(String text) {
        clientViewC3.addTextToTextArea(text);
        clientViewC3.repaint();
    }

    @Override
    public void update(boolean status) {
        clientViewC3.setSportButtonStatus(status);
        clientViewC3.setCookingButtonStatus(status);
        clientViewC3.setHarryPotterButtonStatus(status);
        clientViewC3.repaint();
    }

    class SportButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientViewC3.getPubText(), "", RabbitMQConfigurationC3.ROUTING_KEY_SPORT);
            publisherC3.sendMessage(message, RabbitMQConfigurationC3.ROUTING_KEY_SPORT);
        }
    }

    class CookingButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientViewC3.getPubText(), "", RabbitMQConfigurationC3.ROUTING_KEY_COOKING);
            publisherC3.sendMessage(message, RabbitMQConfigurationC3.ROUTING_KEY_COOKING);
        }
    }

    class HarryPotterListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientViewC3.getPubText(), "", RabbitMQConfigurationC3.ROUTING_KEY_HARRY_POTTER);
            publisherC3.sendMessage(message, RabbitMQConfigurationC3.ROUTING_KEY_HARRY_POTTER);
        }
    }


}
