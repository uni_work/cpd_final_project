package com.claudiutoader.project.server;


import com.claudiutoader.project.configuration.ServerSocketConfigurationC3;
import com.claudiutoader.project.ui.controller.ControllerC3;
import com.claudiutoader.project.ui.controller.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


@Service
public class TokenRingServerC3 implements Subject, CommandLineRunner {
    private BufferedReader input;
    private PrintWriter output;
    private ServerSocket serverIn;
    private Socket serverOut;
    private ControllerC3 controllerC3;
    private boolean status;


    @Autowired
    public TokenRingServerC3(ControllerC3 controllerC3) throws IOException {
        this.controllerC3 = controllerC3;
        serverIn = new ServerSocket(ServerSocketConfigurationC3.PORT_7070);
    }


    public void start() throws IOException, InterruptedException {
        serverOut = null;
        while (true) {
            try {
                serverOut = new Socket(ServerSocketConfigurationC3.IP, ServerSocketConfigurationC3.PORT_5050);
                if (serverOut != null) { break; }
            }
            catch (IOException e) { Thread.sleep(1000); }
        }

        Socket receiver = serverIn.accept();
        input = new BufferedReader(new InputStreamReader(receiver.getInputStream()));
        output = new PrintWriter(serverOut.getOutputStream(), true);

        String token = "";
        notifySubscribers();
        while(true){

            if(token.equalsIgnoreCase("token")){
                status = true;
                notifySubscribers();
                long startTime = System.currentTimeMillis();

                System.out.println("Token received! Server running: " + serverIn.getLocalPort());
                while(System.currentTimeMillis() - startTime < 10000){
                    // change status for 10 seconds
                }

                status = false;
                notifySubscribers();
                output.println("token");

            }
            System.out.println("Waiting for token...");
            token = input.readLine();

        }
    }

    public void stop() throws IOException {
        serverIn.close();
        serverOut.close();
    }

    @Override
    public void notifySubscribers() {
        controllerC3.update(status);
    }

    @Override
    public void run(String... args) throws Exception {
        this.start();
    }
}
