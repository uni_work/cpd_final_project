package com.claudiutoader.project;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.IOException;

@SpringBootApplication
public class ProjectApplication {


    public static void main(String[] args) throws IOException, InterruptedException {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(ProjectApplication.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);



//        TokenRingServer firstServer = new TokenRingServer(controller);
//        firstServer.start();

	}

}
