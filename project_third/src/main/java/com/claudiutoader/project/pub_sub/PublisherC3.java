package com.claudiutoader.project.pub_sub;

import com.claudiutoader.project.configuration.RabbitMQConfigurationC3;
import com.claudiutoader.project.model.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PublisherC3 {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public PublisherC3(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(Message message, String routingKey) {
        rabbitTemplate.convertAndSend(RabbitMQConfigurationC3.TOPIC_EXCHANGE,
                routingKey,
                message,
                m -> {m.getMessageProperties()
                        .getHeaders()
                        .remove("__TypeId__");
                    return m;}
        );
    }

}
