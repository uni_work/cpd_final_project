package com.claudiutoader.project.pub_sub;

import com.claudiutoader.project.configuration.RabbitMQConfigurationC3;
import com.claudiutoader.project.model.Message;
import com.claudiutoader.project.ui.controller.ControllerC3;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ReceiverC3 {

    private final ControllerC3 controllerC3;

    @Autowired
    public ReceiverC3(ControllerC3 controllerC3) {
        this.controllerC3 = controllerC3;
    }

    @RabbitListener(queues = {RabbitMQConfigurationC3.QUEUE_C3},
            containerFactory = "listenerContainerFactory")
    public void receiveMessage(final Message message)  {
        controllerC3.addTextToTextArea(message.toString());
    }

    /*
    public void receiveMessage(final Message message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        controller.addTextToTextArea(message.toString());

        try {
            channel.basicAck(tag, false);
        } catch (Exception e) {
            channel.basicNack(tag, false, true);
        }
    }
    */

}
