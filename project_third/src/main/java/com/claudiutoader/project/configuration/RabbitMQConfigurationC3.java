package com.claudiutoader.project.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfigurationC3 {
    public static final String TOPIC_EXCHANGE = "topic";
//    public static final String TOPIC_EXCHANGE_COOKING = "Cooking";
//    public static final String TOPIC_EXCHANGE_HARRY_POTTER = "Harry_Potter";

    public static final String QUEUE_C1 = "first";
    public static final String QUEUE_C2 = "second";
    public static final String QUEUE_C3 = "third";

    public static final String ROUTING_KEY_SPORT = "sport";
    public static final String ROUTING_KEY_COOKING = "cooking";
    public static final String ROUTING_KEY_HARRY_POTTER = "harry_potter";

    @Bean
    public Queue firstQueue() {
        return new Queue(QUEUE_C1);
    }

    @Bean
    public Queue secondQueue() { return new Queue(QUEUE_C2); }

    @Bean
    public Queue thirdQueue() { return new Queue(QUEUE_C3); }

    @Bean
    public TopicExchange topic() {
        return new TopicExchange(TOPIC_EXCHANGE);
    }

    @Bean
    public Binding bindingFirstA() {
        return BindingBuilder.bind(firstQueue()).to(topic()).with(ROUTING_KEY_COOKING);
    }

    @Bean
    public Binding bindingFirstB() {
        return BindingBuilder.bind(firstQueue()).to(topic()).with(ROUTING_KEY_HARRY_POTTER);
    }

    @Bean
    public Binding bindingSecondA() {
        return BindingBuilder.bind(secondQueue()).to(topic()).with(ROUTING_KEY_SPORT);
    }

    @Bean
    public Binding bindingSecondB() {
        return BindingBuilder.bind(secondQueue()).to(topic()).with(ROUTING_KEY_COOKING);
    }

    @Bean
    public Binding bindingSecondC() {
        return BindingBuilder.bind(secondQueue()).to(topic()).with(ROUTING_KEY_HARRY_POTTER);
    }

    @Bean
    public Binding bindingThirdA() {
        return BindingBuilder.bind(thirdQueue()).to(topic()).with(ROUTING_KEY_COOKING);
    }

    @Bean
    public Binding bindingThirdB() {
        return BindingBuilder.bind(thirdQueue()).to(topic()).with(ROUTING_KEY_HARRY_POTTER);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory listenerContainerFactory(ConnectionFactory connectionFactory) {
        final SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(messageConverter());
        return factory;
    }


    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
