package com.claudiutoader.project.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServerSocketConfigurationC3 {
    public static String IP = "127.0.0.1";
    public static int PORT_5050 = 5050;
    public static int PORT_6060 = 6060;
    public static int PORT_7070 = 7070;

    @Bean
    public String windowName() {
        return "Third";
    }
}
