package com.claudiutoader.project.ui.controller;

import com.claudiutoader.project.configuration.RabbitMQConfigurationC1;
import com.claudiutoader.project.model.Message;
import com.claudiutoader.project.pub_sub.Publisher;
import com.claudiutoader.project.ui.view.ClientView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@Component
public class Controller implements Observer{
    private ClientView clientView;
    private Publisher publisher;

    @Autowired
    public Controller(ClientView clientView, Publisher publisher) {
        this.clientView = clientView;
        this.publisher = publisher;

        clientView.sportButtonListener(new SportButtonListener());
        clientView.cookingButtonListener(new CookingButtonListener());
        clientView.harryPotterButtonListener(new HarryPotterListener());
    }

    public void addTextToTextArea(String text) {
        clientView.addTextToTextArea(text);
        clientView.repaint();
    }

    @Override
    public void update(boolean status) {
        clientView.setSportButtonStatus(status);
        clientView.setCookingButtonStatus(status);
//        clientView.setHarryPotterButtonStatus(status);
        clientView.repaint();
    }

    class SportButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientView.getPubText(), "", RabbitMQConfigurationC1.ROUTING_KEY_SPORT);
            publisher.sendMessage(message, RabbitMQConfigurationC1.ROUTING_KEY_SPORT);
        }
    }

    class CookingButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientView.getPubText(), "", RabbitMQConfigurationC1.ROUTING_KEY_COOKING);
            publisher.sendMessage(message, RabbitMQConfigurationC1.ROUTING_KEY_COOKING);
        }
    }

    class HarryPotterListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Message message = new Message(clientView.getPubText(), "", RabbitMQConfigurationC1.ROUTING_KEY_HARRY_POTTER);
            publisher.sendMessage(message, RabbitMQConfigurationC1.ROUTING_KEY_HARRY_POTTER);
        }
    }


}
