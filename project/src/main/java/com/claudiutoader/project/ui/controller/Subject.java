package com.claudiutoader.project.ui.controller;

public interface Subject {
    void notifySubscribers();
}
