package com.claudiutoader.project.ui.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

@Component
public class ClientView extends JFrame {
    private JPanel mainPanel;
    private JButton sportButton;
    private JButton cookingButton;
    private JButton harryPotterButton;
    private JTextArea subTextArea;
    private JTextArea pubTextArea;

    @Autowired
    public ClientView(String title) {
        mainPanel = new JPanel();
        sportButton = new JButton("Sport");
        cookingButton = new JButton("Cooking");
        harryPotterButton = new JButton("Harry Potter");
        subTextArea = new JTextArea();
        pubTextArea = new JTextArea();

        harryPotterButton.setEnabled(false);
        subTextArea.setEditable(false);
        subTextArea.setPreferredSize(new Dimension(600, 300));
        pubTextArea.setPreferredSize(new Dimension(600, 50));

        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        JPanel buttonsPanel = new JPanel();
        JPanel textAreaPanel = new JPanel();

        buttonsPanel.setLayout(new FlowLayout());
        buttonsPanel.add(sportButton);
        buttonsPanel.add(cookingButton);
        buttonsPanel.add(harryPotterButton);
        textAreaPanel.add(subTextArea);
        buttonsPanel.add(pubTextArea);

        mainPanel.add(textAreaPanel);
        mainPanel.add(buttonsPanel);

        this.setLocation(400, 100);
        this.setPreferredSize(new Dimension(800, 550));
        this.setContentPane(mainPanel);
        this.pack();
        this.setTitle(title);

        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void sportButtonListener(ActionListener actionListener) {
        sportButton.addActionListener(actionListener);
    }

    public void cookingButtonListener(ActionListener actionListener) {
        cookingButton.addActionListener(actionListener);
    }

    public void harryPotterButtonListener(ActionListener actionListener) {
        harryPotterButton.addActionListener(actionListener);
    }

    public String getPubText() {
        return pubTextArea.getText();
    }

    public void setSubText(String s) {
        subTextArea.setText(s);
    }

    public void setSportButtonStatus(boolean status) {
        sportButton.setEnabled(status);
    }

    public void setCookingButtonStatus(boolean status) {
        cookingButton.setEnabled(status);
    }

    public void setHarryPotterButtonStatus(boolean status) {
        harryPotterButton.setEnabled(status);
    }

    public void addTextToTextArea(String text) {
        subTextArea.append(text);
    }

}
