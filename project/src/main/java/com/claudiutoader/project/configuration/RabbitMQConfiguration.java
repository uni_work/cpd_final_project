//package com.claudiutoader.project.configuration;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
//import org.springframework.amqp.support.converter.MessageConverter;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class RabbitMQConfiguration {
//    public static final String TOPIC_EXCHANGE_SPORT = "Sport";
//    public static final String TOPIC_EXCHANGE_COOKING = "Cooking";
//    public static final String TOPIC_EXCHANGE_HARRY_POTTER = "Harry_Potter";
//
//    public static final String QUEUE_SPORT = "cpd_queue_sport";
//    public static final String QUEUE_COOKING = "cpd_queue_cooking";
//    public static final String QUEUE_HARRY_POTTER = "cpd_queue_harry_potter";
//
//    public static final String ROUTING_KEY = "topics.#";
//    public static final String ROUTING_KEY_C1 = "cooking.harry_potter";
//    public static final String ROUTING_KEY_C2 = "topics.#";
//    public static final String ROUTING_KEY_C3 = "topics.#";
//
//    @Bean
//    public Queue queueSport() {
//        return new Queue(QUEUE_SPORT);
//    }
//
//    @Bean
//    public Queue queueCooking() { return new Queue(QUEUE_COOKING); }
//
//    @Bean
//    public Queue queueHarryPotter() { return new Queue(QUEUE_HARRY_POTTER); }
//
//    @Bean
//    public TopicExchange sportTopicExchange() {
//        return new TopicExchange(TOPIC_EXCHANGE_SPORT);
//    }
//
//    @Bean
//    public TopicExchange cookingTopicExchange() {
//        return new TopicExchange(TOPIC_EXCHANGE_COOKING);
//    }
//
//    @Bean
//    public TopicExchange harryPotterTopicExchange() {
//        return new TopicExchange(TOPIC_EXCHANGE_HARRY_POTTER);
//    }
//
//    @Bean
//    public Binding bindingSport() {
//        return BindingBuilder.bind(queueSport()).to(sportTopicExchange()).with(ROUTING_KEY);
//    }
//
//    @Bean
//    public Binding bindingCooking() {
//        return BindingBuilder.bind(queueCooking()).to(cookingTopicExchange()).with(ROUTING_KEY);
//    }
//
//    @Bean
//    public Binding bindingHarryPotter() {
//        return BindingBuilder.bind(queueHarryPotter()).to(harryPotterTopicExchange()).with(ROUTING_KEY);
//    }
//
//    @Bean
//    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
//        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
//        rabbitTemplate.setMessageConverter(messageConverter());
//        return rabbitTemplate;
//    }
//
//    @Bean
//    public SimpleRabbitListenerContainerFactory listenerContainerFactory(ConnectionFactory connectionFactory) {
//        final SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
//        factory.setConnectionFactory(connectionFactory);
//        factory.setMessageConverter(messageConverter());
//        return factory;
//    }
//
//
//    @Bean
//    public MessageConverter messageConverter() {
//        return new Jackson2JsonMessageConverter();
//    }
//}
