package com.claudiutoader.project.pub_sub;

import com.claudiutoader.project.configuration.RabbitMQConfigurationC1;
import com.claudiutoader.project.model.Message;
import com.claudiutoader.project.ui.controller.Controller;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class Receiver {

    private final Controller controller;

    @Autowired
    public Receiver(Controller controller) {
        this.controller = controller;
    }

    @RabbitListener(queues = {RabbitMQConfigurationC1.QUEUE_C1},
            containerFactory = "listenerContainerFactory")
    public void receiveMessage(final Message message)  {
        controller.addTextToTextArea(message.toString());
    }

    /*
    public void receiveMessage(final Message message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        controller.addTextToTextArea(message.toString());

        try {
            channel.basicAck(tag, false);
        } catch (Exception e) {
            channel.basicNack(tag, false, true);
        }
    }
    */

}
