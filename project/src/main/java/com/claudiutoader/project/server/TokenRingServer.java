package com.claudiutoader.project.server;


import com.claudiutoader.project.configuration.ServerSocketConfiguration;
import com.claudiutoader.project.ui.controller.Controller;
import com.claudiutoader.project.ui.controller.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


@Service
public class TokenRingServer implements Subject, CommandLineRunner {
//    private final RabbitTemplate rabbitTemplate;
    private BufferedReader input;
    private PrintWriter output;
    private ServerSocket serverIn;
    private Socket serverOut;
    private Controller controller;
    private boolean status;


    @Autowired
    public TokenRingServer(Controller controller) throws IOException {
        this.controller = controller;
        serverIn = new ServerSocket(ServerSocketConfiguration.PORT_5050);
    }


    public void start() throws IOException, InterruptedException {
        serverOut = null;
        while (true) {
            try {
                serverOut = new Socket(ServerSocketConfiguration.IP, ServerSocketConfiguration.PORT_6060);
                if (serverOut != null) { break; }
            }
            catch (IOException e) { Thread.sleep(1000); }
        }

        Socket receiver = serverIn.accept();
        input = new BufferedReader(new InputStreamReader(receiver.getInputStream()));
        output = new PrintWriter(serverOut.getOutputStream(), true);

        String token = "token";
        while(true){

            if(token.equalsIgnoreCase("token")){
                status = true;
                notifySubscribers();

                long startTime = System.currentTimeMillis();

                System.out.println("Token received! Server running: " + serverIn.getLocalPort());
                while(System.currentTimeMillis() - startTime < 10000){
                    // change status for 10 seconds
                }

                status = false;
                notifySubscribers();
                output.println("token");

            }
            System.out.println("Waiting for token...");
            token = input.readLine();

        }
    }

    public void stop() throws IOException {
        serverIn.close();
        serverOut.close();
    }

    @Override
    public void notifySubscribers() {
        controller.update(status);
    }

    @Override
    public void run(String... args) throws Exception {
        this.start();
    }
}
