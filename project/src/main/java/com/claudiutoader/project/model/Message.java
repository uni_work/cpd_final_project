package com.claudiutoader.project.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Message implements Serializable {
    private String message;
    private String dateTime;
    private String topic;

    public Message(@JsonProperty("message") String message,
                   @JsonProperty("date_time") String dateTime,
                   @JsonProperty("topic") String topic) {
        this.message = message;
        this.dateTime = dateTime;
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return message + " : " + topic + "\n";
    }
}
